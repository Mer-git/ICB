# -*- coding: utf-8 -*-
"""
Editor de Spyder

Este es un archivo temporal.
"""

#### punto 1
def sonIguales (lista, l, n):
    res=True
    k=1
    while (res==True and k<n):
        if lista[l]==lista[l+k]:
            k+=1
        else:
            res=False
    return res

def hayBorde (lista, n, h):
    res=False
    i=n-1
    while (res==False and i<len(lista)-1):
        if abs(lista[i]-lista[i+1])==h:
            if sonIguales(lista,i-(n-1),n) and sonIguales(lista,i+1,n):
                res=True
            else:
                i+=1
        else:
            i+=1
    return res

print(hayBorde([1,3,3,3,5,5,5,4],3,2))

#### punto 2
def listaTriangular(lista):
    cuenta=0
    i=1
    res=False
    while (cuenta<=1 and i<len(lista)-1):
        if lista[i]>lista[i-1] and lista[i]>lista[i+1]:
            cuenta+=1
        i+=1
    if cuenta<=1: res=True
    return res

print(listaTriangular([2,2,2,2]))



