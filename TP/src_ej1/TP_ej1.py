# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 14:50:02 2019

@author: mmabb
"""
import random
import time
import sys
import matplotlib.pyplot as plt
import numpy as np
import csv

## para abrir lista puntos desde txt
def listaDePuntos(fn):
    arch_entrada = fn#[0]

    with open(arch_entrada) as f:
        ldt = [tuple(map(float, i.split(' '))) for i in f]
    return ldt

# =============================================================================
# ## generar lista de puntos con valores entre 1 y 1000 (R DETERMINA LONGITUD LISTA)
# def gen_points_mil(r):
#     a=[]
# 
#     for i in range(1,r):
#         a.append( (random.randint(1,1000), random.randint(1,1000)) )
#     return a
# =============================================================================

## generar lista de puntos con valores entre 0 y 1 ((R DETERMINA LONGITUD LISTA))
def gen_points(r):
    a=[]

    for i in range(1,r):
        a.append( (random.random(), random.random()) )
    return a


#distancia dandole 2 puntos
def distancia(p1 , p2):
    d=((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)**(0.5)
    return d

#distancia dandole una lista con 2 puntos
def distancial2p(l):
    d=((l[0][0]-l[1][0])**2 + (l[0][1]-l[1][1])**2)**(0.5)
    return d

#fuerza bruta
def distanciaMinima (l):
    mindist= distancia(l[0], l[1])
    minpair= l[0], l[1]
    for i in range(len(l)):
        for j in range(len(l)):
          if i!=j:  
            if distancia(l[i], l[j]) < mindist:
                    mindist= distancia(l[i], l[j])
                    minpair= l[i], l[j]
    return minpair
    #return mindist


# fn principal
def distanciaMinimaDyC(l, algoritmo):
    if algoritmo=='python':
        l.sort()
    if algoritmo=='merge':
        mergeSort(l)
    if algoritmo=='up':
        upsort(l)
    return distanciarec(l)
        
        
        
## D y C        
def distanciarec (a):
    n=len(a)
    if n == 3:
            return distanciaMinima(a)
    if n==2:
            return a
    mid = n//2 
    

    dis_left  = distanciarec(a[:mid])
    dis_right = distanciarec(a[mid:])
    
    #min_dis = min(dis_left, dis_right)
    min_dis = dis_left if distancial2p(dis_left)< distancial2p(dis_right) else  dis_right
   
    xmedio=dis_left[len(dis_left)-1][0]
    medio=[]
    for p in dis_left:
        if abs(p[0]-xmedio)<distancial2p(min_dis):
            medio.append(p)
    for p in dis_right:
        if abs(p[0]-xmedio)<distancial2p(min_dis):
            medio.append(p)
    
    if len(medio)<2:
        return min_dis
    else:
        minmedio= distanciaMinima(medio)
        if distancial2p(minmedio)<distancial2p(min_dis):
            min_dis=minmedio
    
        
    return min_dis

## algoritmos para ordenar la lista de puntos
# merge sort
def mergeSort(alist):

   #print("Splitting ",alist)

   if len(alist)>1:
       mid = len(alist)//2
       lefthalf = alist[:mid]
       righthalf = alist[mid:]

       #recursion
       mergeSort(lefthalf)
       mergeSort(righthalf)

       i=0
       j=0
       k=0

       while i < len(lefthalf) and j < len(righthalf):
           if lefthalf[i] < righthalf[j]:
               alist[k]=lefthalf[i]
               i=i+1
           else:
               alist[k]=righthalf[j]
               j=j+1
           k=k+1

       while i < len(lefthalf):
           alist[k]=lefthalf[i]
           i=i+1
           k=k+1

       while j < len(righthalf):
           alist[k]=righthalf[j]
           j=j+1
           k=k+1

   #print("Merging ",alist)
        
 
# upsort
def upsort(a):
    actual=len(a)-1
    m=0
    while actual>0:
        m=maxPos(a,0,actual)
        a[m], a [actual]=a[actual], a[m]
        actual -=1
        
# accesorio de upsort
def maxPos(l, i, der):
    lista=l[i:(der+1)]
    return lista.index(max(lista))


## para evaluar la performance
def evaluar():
    ls=[500,1000,1500,2000,2500,3000]
    
    tfbruta=[]
    for i in range(len(ls)):
        test=gen_points(ls[i]-1)
        start = time.time()
        distanciaMinima(test)
        end = time.time()
        diftime= end-start
        print("tiempo f bruta = %s" % (end - start))#, 'res = %s' % (r))
        tfbruta.append(diftime)

    tDyCpython=[]
    for i in range(len(ls)):
        test=gen_points(ls[i]-1)
        start = time.time()
        distanciaMinimaDyC(test, 'python')  
        end = time.time()
        diftime= end-start
        print("tiempo D y C python = %s" % (end - start))#, 'res = %s' % (r))
        tDyCpython.append(diftime)

    tDyCup=[]
    for i in range(len(ls)):
        test=gen_points(ls[i]-1)
        start = time.time()
        distanciaMinimaDyC(test, 'up')
        end = time.time()
        diftime= end-start
        print("tiempo D y C up = %s" % (end - start))#, 'res = %s' % (r))
        tDyCup.append(diftime)

    tDyCmerge=[]
    for i in range(len(ls)):
        test=gen_points(ls[i]-1)
        start = time.time()
        distanciaMinimaDyC(test, 'merge')
        end = time.time()
        diftime= end-start
        print("tiempo D y C merge = %s" % (end - start))#, 'res = %s' % (r))
        tDyCmerge.append(diftime)
    
        
    figure = plt.figure()
    plt.title('Tiempo de ejecución')
    plt.yscale('log')
    plt.xlim(450,3010)
    plt.ylim(10**-4,15)
    
    plt.plot(ls, tfbruta, label='Fuerza bruta')
    plt.plot(ls, tDyCpython, label='D&C Python')
    plt.plot(ls, tDyCup, label='D&C up')
    plt.plot(ls, tDyCmerge, label='D&C merge')
    
    plt.legend(loc='best')
    plt.xlabel('Tamaño de lista [N]')
    plt.ylabel('Tiempo de ejecución [seg]')

    plt.savefig('Tiempo de ejecucion')

