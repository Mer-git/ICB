# -*- coding: utf-8 -*-
"""
Created on Fri Dec  6 10:56:56 2019

@author: jbizz
"""


class Laberinto(object):
    def __init__(self, parent=None):
        self.parent = parent
        self.grilla=[]
        self.dimension=None
        self.posicionRata=()
        self.posicionQueso=()
        self.laberinto=[]
        self.posicionRataAnterior=()
        self.pasos=0
        self.CaminoActual = []
        

    def cargar(self, fn):
        with open(fn, 'r') as entrada : 
            lab = []                      
            for linea in entrada:
                linea = linea.strip('\n')             
                lab.append(linea)  
            fil=int(lab[0][4] + lab[0][5])  #determino tamano basado en primera fila
            col=int(lab[0][7] + lab[0][8])
            self.dimension=(fil,col)
            for i in range(1,fil+1):
                fila=lab[i]
                celdas=[]
                for j in range(0,col):
                    celdas.append(eval(fila[0+ j*9 : 9+j*9])) #cada celda del laberinto en el archivo tiene 9 caracteres
                self.grilla.append(celdas)         
            self.posicionQueso=(fil-1,col-1) #queso al final del laberinto
            self.posicionRata=(0,0)  #rata al principio del laberinto
            self.posicionRataAnterior=(0,0)
            self.resetear()
            self.laberinto=[]
            for i in range(fil): 
                celdas=[]
                for j in range(col):
                    celdas.append({'visitada': False, 'caminoActual': False}) 
                self.laberinto.append(celdas)
           
            
    def tamano(self):
        return self.dimension
    
    def resetear(self):   #para resetear lo caminado (no resetea la posicion de la rata o del queso si lo hubiera movido)
        self.laberinto=[]
        for i in range(self.tamano()[0]): 
                celdas=[]
                for j in range(self.tamano()[1]):
                    celdas.append({'visitada': False, 'caminoActual': False}) 
                self.laberinto.append(celdas)
    
    def getPosicionRata(self):
        return self.posicionRata
    
    def getPosicionQueso(self):
        return self.posicionQueso
    
    def setPosicionRata(self, i, j):
        if i>= self.dimension[0] or j>= self.dimension[1]:
            return False
        else:
            self.posicionRata=(i,j)
            return True
        
    
    def setPosicionQueso(self, i, j):
        if i>= self.dimension[0] or j>= self.dimension[1]:
            return False
        else:
            self.posicionQueso=(i,j)
            return True

    def esPosicionRata(self, i, j):
        return i==self.posicionRata[0] and j==self.posicionRata[1]
    
    def esPosicionqueso(self, i, j):
        return i==self.posicionQueso[0] and j==self.posicionQueso[1]
    
    
    def get(self, i, j): #True si hay borde, en orden izq arriba der abajo
        bordes=[]
        bordes.append(self.grilla[i][j][0]==1)
        bordes.append(self.grilla[i][j][1]==1)
        bordes.append(self.grilla[i][j][2]==1)
        bordes.append(self.grilla[i][j][3]==1)
        return bordes
    
    def getInfoCelda(self, i, j):
        return self.laberinto[i][j]
    
    def resuelto(self):
        if self.posicionQueso==self.posicionRata:
            return True
        else:
            return False
    
    def resolver(self):
        fila=self.getPosicionRata()[0]
        col=self.getPosicionRata()[1]
        self.laberinto[fila][col]['visitada']=True
        self.laberinto[fila][col]['caminoActual']=True
        self.pasos= self.pasos +1
        
        
        if self.resuelto(): #para ver si se encontro con el queso
            return True
       
        #miro a la derecha, solo se movera en esa direccion si no hay borde, si no estuvo ahi, y si no esta en el limite derecho del laberinto
        elif self.getPosicionRata()[1] < (self.dimension[1]-1) and self.get(fila,col)[2]==False and not self.getInfoCelda(fila,col+1)['visitada']:
                self.setPosicionRata(fila,col+1)
                self.CaminoActual.append((fila,col+1))       
                return True and self.resolver()
       
        #miro abajo, solo se movera en esa direccion si no hay borde, si no estuvo ahi, y si no esta en el limite inferior del laberinto
        elif self.getPosicionRata()[0]< (self.dimension[0]-1) and  self.get(fila,col)[3]==False and not self.getInfoCelda(fila+1,col)['visitada']:
            
                self.setPosicionRata(fila+1,col)
                self.CaminoActual.append((fila+1,col))
                return True and self.resolver()
        
        
        #miro izq, solo se movera en esa direccion si no hay borde, si no estuvo ahi, y si no esta en el limite izq del laberinto
        elif self.getPosicionRata()[1]>0 and self.get(fila,col)[0]==False and not self.getInfoCelda(fila,col-1)['visitada']:
            
                self.setPosicionRata(fila,col-1)
                self.CaminoActual.append((fila,col-1))
                return True and self.resolver()
        #miro arriba, solo se movera en esa direccion si no hay borde, si no estuvo ahi, y si no esta en el limite superior o del laberinto
        elif self.getPosicionRata()[0]>0 and self.get(fila,col)[1]==False and not self.getInfoCelda(fila-1,col)['visitada']:
            
                self.setPosicionRata(fila-1,col)
                self.CaminoActual.append((fila-1,col))
                return True and self.resolver()

        #backtrack
        elif len(self.CaminoActual)>1:
            self.CaminoActual.pop() 
            self.setPosicionRata(self.CaminoActual[-1][0],self.CaminoActual[-1][1])
            return True and self.resolver()
        else:
            return False