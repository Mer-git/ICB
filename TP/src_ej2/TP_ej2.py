# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 21:40:31 2019

@author: jbizz
"""

#import imfilters
from scipy import misc
import numpy as np
import PIL
from numba import jit, threading_layer, config
import time
import matplotlib.pyplot as plt


## ESTE ES COMO EL DEL TP, PERO CON LOS VALORES DE LA MULTIPLICACION UN POCO MODIFICADOS, DA MUCHO MAS LINDO!
@jit(parallel=True)
def gray_filter(img):
    entrada=PIL.Image.open(img)
    width, height = entrada.size
    tabla=np.zeros(shape=(width, height))
    for i in range(width):
     for j in range(height):
       pixel = entrada.getpixel((i, j))
       red =   pixel[0]
       green = pixel[1]
       blue =  pixel[2]
       gray = (red * 0.299) + (green * 0.587) + (blue * 0.114)
       tabla[i, j] = (int(gray))
    imgByN = PIL.Image.fromarray(tabla.astype(np.uint8), mode="L")
    imgByN = imgByN.transpose(method=PIL.Image.TRANSPOSE)
    imgByN.save("imgBYN.jpg")
    return imgByN
    
@jit(parallel=True)
def blur_filter(figura):
    width, height = figura.size
    tablablur=np.zeros(shape=(width, height))
    for i in range(1,width-1):
      for j in range(1,height-1):
  
          blur = (figura.getpixel((i-1, j)) + figura.getpixel((i+1, j)) + figura.getpixel((i, j-1)) + figura.getpixel((i, j+1)))/4
            
          tablablur[i, j] = (int(blur))
     
    resblur = PIL.Image.fromarray(tablablur.astype(np.uint8), mode="L") 
    resblur = resblur.transpose(method=PIL.Image.TRANSPOSE)
    resblur.save("resblur.jpg")
    return resblur

# sin paralelizar
start = time.time()
blur_filter(gray_filter('Imagen.jpg'))
end = time.time()
print("Elapsed = %s" % (end - start))

# paralelizando
tiempos =[]
nucleos = [1,2,3,4]
for i in range(len(nucleos)):
    config.threading_layer= nucleos[i]-1
    start = time.time()
    blur_filter(gray_filter('paisaje.jpg'))
    end = time.time()
    tiempos.append(end-start)
    print("Elapsed = %s" % (end - start))

# grafico
x= [1,2,3,4]
y= tiempos

fig= plt.figure()
plt.title('Escalabilidad')
plt.xlabel('Nucleos')
plt.xticks(range(1,5))
plt.ylabel('Tiempo [seg]')
plt.plot(x,y)

plt.savefig('Escalabilidad')











