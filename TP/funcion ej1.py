# -*- coding: utf-8 -*-
"""
Created on Thu Dec  5 12:04:19 2019

@author: jbizz
"""

## ultimo que hice armado en funcion

def distancial2p(l):
    d=((l[0][0]-l[1][0])**2 + (l[0][1]-l[1][1])**2)**(0.5)
    return d

def distancia(p1 , p2):
    d=((p1[0]-p2[0])**2 + (p1[1]-p2[1])**2)**(0.5)
    return d


lista=[(1.3,4.4), (-1.0 , 5.3) , (-0.45 , 5.4) , (-0.5 , 5.5)]
lista.sort()


def closest(l):
    mid=len(l)//2
    lizq=lista[:mid]
    lder=lista[mid:]
    dist_lizq=distancial2p(lizq)
    dist_lder=distancial2p(lder)

    if dist_lizq<dist_lder:
        min_dist_puntos=lizq
        min_dist=dist_lizq
    else:
        min_dist_puntos=lder
        min_dist=dist_lder

    distx=lder[0][0]-lizq[mid-1][0]

    if min_dist>distx:
        distborde=distancia(lizq[mid-1], lder[0])
        if distborde<distancial2p(min_dist_puntos):
            min_dist_puntos=[lizq[mid-1], lder[0]]
    
    return min_dist_puntos
    
    