# -*- coding: utf-8 -*-
"""
Created on Sun Oct 27 15:15:20 2019

@author: mmabb
"""
import random

def generarMazo(n):
    mazo=[1,2,3,4,5,6,7,8,9,10,11,12,13]*4
    m=mazo*n
    random.shuffle(m)
    return m

def jugar(m):
    suma=0
    while suma<21:
        if len(m)>0:
            carta=m.pop(0)
            suma=suma+carta
        else:
            return suma
    return suma

def jugar_varios(m, j):
    ronda=[]
    for i in range(j):
        ronda.append(jugar(m))
    return ronda

print(jugar_varios(generarMazo(4),4))

def jugarMiedo(m):
    suma=0
    while suma<19:
        if len(m)>0:
            carta=m.pop(0)
            suma=suma+carta
        else:
            return suma
    return suma
    
def jugarBorracho(m):
    suma=0
    borracho=random.random()
    while suma<21 and borracho>0.5:
        if len(m)>0:
            carta=m.pop(0)
            suma=suma+carta
            borracho=random.random()
        else:
            return suma
    return suma

print(jugarBorracho(generarMazo(4))

