# -*- coding: utf-8 -*-
"""
Created on Wed Dec 11 08:37:31 2019

@author: mmabb
"""

import numpy as np
import time
import random
import sys

global N
N = 2000

def matrizCeros():
    matriz = []
    for i in range(N):
        matriz.append([])
        for j in range(N):
            matriz[i].append(0.0)
    #print(matriz)
    return matriz
    pass

def matrizAleatoria(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz)):
            matriz[i][j]=random.random()
    #print(matriz)
    pass

def multMatrices(a, b, c):
    if isinstance(a, np.matrix) and isinstance(b, np.matrix):
        np.copyto(c,np.matmul(a,b))
    else:    
        for i in range(len(a)):
            for j in range(len(b)):
                for k in range (len(b)):
                    c[i][j]+=a[i][k]*b[k][j]
    pass   

def medirTiempos(fn, *args):
    tiempo_i = time.time()
    fn(*args)
    tiempo_dif = time.time() - tiempo_i 
    return tiempo_dif
    pass
    
def realizarExperimento():
    a= matrizCeros()
    b= matrizCeros()
    c= matrizCeros()
    
    matrizAleatoria(a)
    matrizAleatoria(b)
    
    tiempo= medirTiempos(multMatrices,a,b,c)
    print("Tiempo total listas: %f.s" % tiempo)

    A= np.matrix(a)
    B= np.matrix(b)
    C= np.matrix(c)
    
    tiempo_np= medirTiempos(multMatrices,A,B,C)
    print("Tiempo total numpy: %f.s" % tiempo_np)    
    pass



if __name__ == '__main__':
    if len(sys.argv) > 1:
        N = int(sys.argv[1])
    realizarExperimento()
