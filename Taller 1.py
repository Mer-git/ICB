# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 08:49:40 2019

@author: mmabb
"""

import math

#Función para chequear si el número (res) es primo
def es_primo(x):
    primo=0
    for i in range(1,(x+1)):
        if (x%i==0):
            primo+=1
    return primo==2
        

#Función para pasar los valores de verdad a números
def beta(x):
    if (x):
        return 1
    else:
        return 0

#Funcion para calcular el predicado auxiliar 
def es2N1(x):
    a=(math.log(x+1))/math.log(2)
    return a%1==0

#para caluclar la sumatoria
def Sumatoria (x):
    i=1
    suma=0
    while i<(x+1):
        suma=suma+(1*beta(es_primo(i))*beta(es2N1(i)))
        i=i+1
    return suma


def Resacon(n):
    pot=n
    res=(2**pot)- 1
    while beta(es_primo(res))==0 and Sumatoria(res)!=n:
            pot=pot+1
            res=(2**pot)-1
    return res


print(Resacon(7))